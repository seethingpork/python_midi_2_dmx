import chroma

# class for handling color mixing and output
class MidiColor:
    def __init__(self, no_of_colors):
        self.output_color = chroma.Color('#000000')
        self.no_of_colors = no_of_colors
        self.lookup_table = {}
        for j in range(0, self.no_of_colors):
            h = j * (360 - 0) / self.no_of_colors + 0
            self.lookup_table[j] = chroma.Color('#000000')
            self.lookup_table[j].hsv = (h, 1, 0.5)

        self.color_counts = {}
        for j in range(0, self.no_of_colors):
            self.color_counts[j] = 0

    def add_color(self, color_ref):
        self.color_counts[color_ref] += 1
        self.color_counts[color_ref] = self._clamp(self.color_counts[color_ref], 0, 5)
        self._mix_colors()

    def rem_color(self, color_ref):
        self.color_counts[color_ref] -= 1
        self.color_counts[color_ref] = self._clamp(self.color_counts[color_ref], 0, 5)
        self._mix_colors()

    def _mix_colors(self):
        self.output_color.rgb = (0, 0, 0)
        for i in range(0, self.no_of_colors):
            if self.color_counts[i]:
                self.output_color = self.output_color + self.lookup_table[i]

    def _clamp(self, n, minn, maxn):
        return max(min(maxn, n), minn)
