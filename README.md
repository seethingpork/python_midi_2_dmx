Python to MIDI 2 DMX
====================

## Background

The original motivation for creating this code was to convert MIDI intput into DMX output for controlling stage lighting for the internation year of light show in 2015.

## Dependencies

### [Python2.7](https://www.python.org/)
  - Grab the latest release from the python.org website

### [chroma](https://github.com/seenaburns/Chroma)
  - Use pip to install
  - For managing colors
  
### [pycharm](http://www.pygame.org/news.html)
  - Didn't have any luck using pip to install this one. Best to just follow install instructions on the website.
  - Primariy for reading MIDI input (although has many other useful features)
  
### [pysimpledmx](https://github.com/c0z3n/pySimpleDMX)
  - Already included in this repository, no need to download or install
  - For generating DMX output